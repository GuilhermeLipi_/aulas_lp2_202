/** 

*@param ctrl
*@param func
*@returns {String}

*/
function baseURL(uri){
    return 'http://' + window.location.hostname +
        '/lp2/Guilherme_Lipi' + (uri ? '/' + uri : '');
}

function api(ctrl, func){
    return baseURL('api/'+ctrl + 'rest/' + func);
}