<div class="container">
      <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
        <div class="text-center">
            
            <h3 class="mb-2">Controle Financeiro Pessoal</h3>

            <form class="border border-light p-5" method="POST">
            <p class="h4 mb-4">Entrar</p>
            <div class="form-outline mb-4">
                <input type="email" name="email" id="email" class="form-control" />
                <label class="form-label" for="form1Example1">Email</label>
            </div>
            <div class="form-outline mb-4">
                <input type="password" id="senha" name="senha" class="form-control" />
                <label class="form-label" for="form1Example2">Senha</label>
            </div>
            <div class="row mb-4">
                <div class="col d-flex justify-content-center">
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Enviar</button>
            <p class="red-text"><?= $error ? 'Dados de acesso incorretos.': '' ?></p>
            
            </form>

        </div>
    </div>
</div>