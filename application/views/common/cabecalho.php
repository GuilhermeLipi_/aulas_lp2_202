<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>LPII 2020/3</title>
    
    <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"/>
    <link rel="stylesheet" href="<?= base_url()?>Assets/MDB/css/mdb.min.css" />
    <link rel="stylesheet" href="<?= base_url()?>Assets/MDB/css/style.css">
    <link rel="stylesheet" href="<?= base_url()?>Assets/MDB/css/bootstrap.min.css">
    
    <script type="text/javascript" src="<?= base_url()?>Assets/MDB/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>Assets/MDB/js/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>Assets/MDB/js/bootstrap.min.js"></script>
    

</head>
  <body>